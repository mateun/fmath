#pragma once

#ifdef FMATH_EXPORTS
#define FMATH_API __declspec(dllexport)
#else
#define FMATH_API __declspec(dllimport)
#endif

#include <vector>
using namespace std;





class Mat44 {
public:
	FMATH_API  Mat44();
	FMATH_API  Mat44(float* values);
	FMATH_API void  values(float* valuptr);
	FMATH_API Mat44 operator* (const Mat44& other);

private:
	vector<vector<float>> _items;
	void initItems();
};




