#include "pch.h"
#include "fmath.h"
#include <vector>



Mat44::Mat44() {
	initItems();
}

void Mat44::initItems() {
	vector<float> row1{ 1, 0, 0, 0 };
	vector<float> row2{ 0, 1, 0, 0 };
	vector<float> row3{ 0, 0, 1, 0 };
	vector<float> row4{ 0, 0, 1, 0 };

	_items.push_back(row1);
	_items.push_back(row2);
	_items.push_back(row3);
	_items.push_back(row4);
}

Mat44::Mat44(float* values) {
	initItems();
	int c = 0;
	for (int r = 0; r < 4; r++) {
		for (int i = 0; i < 4; i++) {
			_items[r][i] = values[c++];
		}
		
	}
	
}

void Mat44::values(float* valueptr) {
	
	int count = 0;
	for (auto r : _items) {
		valueptr[count++] = r[0];
		valueptr[count++] = r[1];
		valueptr[count++] = r[2];
		valueptr[count++] = r[3];
	}
}

Mat44 Mat44::operator*(const Mat44& other) {
	Mat44 m;
	m._items[0][0] = this->_items[0][0] * other._items[0][0] + this->_items[0][1] * other._items[1][0] + this->_items[0][2] * other._items[2][0] + this->_items[0][3] * other._items[3][0];
	m._items[0][1] = this->_items[0][0] * other._items[0][1] + this->_items[0][1] * other._items[1][1] + this->_items[0][2] * other._items[2][1] + this->_items[0][3] * other._items[3][1];
	m._items[0][2] = this->_items[0][0] * other._items[0][2] + this->_items[0][1] * other._items[1][2] + this->_items[0][2] * other._items[2][2] + this->_items[0][3] * other._items[3][2];
	m._items[0][3] = this->_items[0][0] * other._items[0][3] + this->_items[0][1] * other._items[1][3] + this->_items[0][2] * other._items[2][3] + this->_items[0][3] * other._items[3][3];

	m._items[1][0] = this->_items[1][0] * other._items[0][0] + this->_items[1][1] * other._items[1][0] + this->_items[1][2] * other._items[2][0] + this->_items[1][3] * other._items[3][0];
	m._items[1][1] = this->_items[1][0] * other._items[0][1] + this->_items[1][1] * other._items[1][1] + this->_items[1][2] * other._items[2][1] + this->_items[1][3] * other._items[3][1];
	m._items[1][2] = this->_items[1][0] * other._items[0][2] + this->_items[1][1] * other._items[1][2] + this->_items[1][2] * other._items[2][2] + this->_items[1][3] * other._items[3][2];
	m._items[1][3] = this->_items[1][0] * other._items[0][3] + this->_items[1][1] * other._items[1][3] + this->_items[1][2] * other._items[2][3] + this->_items[1][3] * other._items[3][3];

	m._items[2][0] = this->_items[2][0] * other._items[0][0] + this->_items[2][1] * other._items[1][0] + this->_items[2][2] * other._items[2][0] + this->_items[2][3] * other._items[3][0];
	m._items[2][1] = this->_items[2][0] * other._items[0][1] + this->_items[2][1] * other._items[1][1] + this->_items[2][2] * other._items[2][1] + this->_items[2][3] * other._items[3][1];
	m._items[2][2] = this->_items[2][0] * other._items[0][2] + this->_items[2][1] * other._items[1][2] + this->_items[2][2] * other._items[2][2] + this->_items[2][3] * other._items[3][2];
	m._items[2][3] = this->_items[2][0] * other._items[0][3] + this->_items[2][1] * other._items[1][3] + this->_items[2][2] * other._items[2][3] + this->_items[2][3] * other._items[3][3];

	m._items[3][0] = this->_items[3][0] * other._items[0][0] + this->_items[3][1] * other._items[1][0] + this->_items[3][2] * other._items[2][0] + this->_items[3][3] * other._items[3][0];
	m._items[3][1] = this->_items[3][0] * other._items[0][1] + this->_items[3][1] * other._items[1][1] + this->_items[3][2] * other._items[2][1] + this->_items[3][3] * other._items[3][1];
	m._items[3][2] = this->_items[3][0] * other._items[0][2] + this->_items[3][1] * other._items[1][2] + this->_items[3][2] * other._items[2][2] + this->_items[3][3] * other._items[3][2];
	m._items[3][3] = this->_items[3][0] * other._items[0][3] + this->_items[3][1] * other._items[1][3] + this->_items[3][2] * other._items[2][3] + this->_items[3][3] * other._items[3][3];

	return m;
}