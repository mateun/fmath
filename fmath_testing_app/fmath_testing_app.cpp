// fmath_testing_app.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <fmath.h>

int main()
{
   
    Mat44 mat;
    float val[16];
    mat.values(val);
    for (int i = 0; i < 16; i++) {
        if (i % 4 == 0) printf("\n");
        printf("val[%d]: %f\t", i, val[i]);

    }

    printf("--------------------------------------------------------------------------------------\n");

    Mat44 mat2;
    Mat44 mat3 = mat * mat2;
    mat3.values(val);
    for (int i = 0; i < 16; i++) {
        if (i % 4 == 0) printf("\n");
        printf("val[%d]: %f\t", i, val[i]);

    }
    printf("--------------------------------------------------------------------------------------\n");

    Mat44 mat4(new float[16]{ 2, 0,0, 0, 3, 0, 0, 0, 4, 0, 0, 0, 6, 7, 8, 9 });
    mat4.values(val);
    for (int i = 0; i < 16; i++) {
        if (i % 4 == 0) printf("\n");
        printf("val[%d]: %f\t", i, val[i]);

    }

    Mat44 mat5 = mat4 * mat;
    mat5.values(val);
    for (int i = 0; i < 16; i++) {
        if (i % 4 == 0) printf("\n");
        printf("val[%d]: %f\t", i, val[i]);

    }

    printf("test ended\n");
}